public class Vertex131 {
    public char label; // label (e.g. ‘A’)
    public boolean wasVisited;

    // ------------------------------------------------------------
    public Vertex131(char lab) // constructor
    {
        label = lab;
        wasVisited = false;
    }
    // ------------------------------------------------------------
}
