public class Vertex133 {
    public char label; // label (e.g. ‘A’)
    public boolean wasVisited;

    // -------------------------------------------------------------
    public Vertex133(char lab) // constructor
    {
        label = lab;
        wasVisited = false;
    }
    // -------------------------------------------------------------
}
