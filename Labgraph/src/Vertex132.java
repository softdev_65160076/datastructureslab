public class Vertex132 {
    public char label; // label (e.g. ‘A’)
    public boolean wasVisited;

    // -------------------------------------------------------------
    public Vertex132(char lab) // constructor
    {
        label = lab;
        wasVisited = false;
    }
    // -------------------------------------------------------------
}
