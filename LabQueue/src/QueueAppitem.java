public class QueueAppitem {
    public static void main(String[] args) {
        Queueitem theQueueitem = new Queueitem(5); 
        theQueueitem.insert(10); 
        theQueueitem.insert(20);
        theQueueitem.insert(30);
        theQueueitem.insert(40);

        theQueueitem.remove(); 
        theQueueitem.remove(); 
        theQueueitem.remove();

        theQueueitem.insert(50);
        theQueueitem.insert(60); 
        theQueueitem.insert(70);
        theQueueitem.insert(80);

        while (!theQueueitem.isEmpty()){
            long n = theQueueitem.remove(); 
            System.out.print(n);
            System.out.print(" ");
        }
        System.out.println("");
    }
}
