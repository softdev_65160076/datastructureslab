class Tree {
    private Node root;

    public Tree() {
        root = null;
    }

    public Node find(int key) {
        Node current = root;
        while (current.iData != key) // while no match,
        {
            if (key < current.iData) // go left?
                current = current.leftChild;
            else
                current = current.rightChild; // or go right?
            if (current == null) // if no child,
                return null; // didn’t find it
        }
        return current; // found it
    }

    public void insert(int id, double dd) {
        Node newNode = new Node(); // Create a new node
        newNode.iData = id; // Insert data
        newNode.fData = dd;
    
        if (root == null) { // No node in the root
            root = newNode;
        } else { // Root is occupied
            Node current = root; // Start at the root
            Node parent;
    
            while (true) { // Loop until insertion
                parent = current;
    
                if (id < current.iData) { // Go left?
                    current = current.leftChild;
    
                    if (current == null) { // If end of the line, insert on the left
                        parent.leftChild = newNode;
                        return;
                    }
                } else { // Or go right?
                    current = current.rightChild;
    
                    if (current == null) { // If end of the line, insert on the right
                        parent.rightChild = newNode;
                        return;
                    }
                }
            } // End while
        } // End else (root is not null)
    }

    public void delete(int id) {
        //
    }

}
