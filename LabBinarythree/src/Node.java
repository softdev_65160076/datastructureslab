class Node {
    int iData;
    double fData;
    Node leftChild;
    Node rightChild;

    public void displayNode() {
        System.out.print("{ " + iData + ", " + fData + " } ");
    }
}
