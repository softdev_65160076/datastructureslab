class Link2 {
    public int iData;      
    public double dData;   
    public Link2 next;     

    
    public Link2(int id, double dd) {
        iData = id;
        dData = dd;
        next = null;  
    }

    
    public void displayLink() {
        System.out.print("{" + iData + ", " + dData + "} ");
    }
}
