class LinkList2 {
    private Link2 first; // อ้างอิงไปยังลิงค์แรกในรายการ

    // คอนสตรักเตอร์
    public LinkList2() {
        first = null; // ไม่มีลิงค์ในรายการเลย
    }

    // เพิ่มลิงค์ที่ตำแหน่งแรกของรายการ
    public void insertFirst(int id, double dd) {
        Link2 newLink = new Link2(id, dd);
        newLink.next = first; // ชี้ไปยังลิงค์ที่เดิมแรก
        first = newLink; // ตอนนี้ first ชี้ไปยังลิงค์นี้
    }

    // ค้นหาลิงค์ที่มีคีย์ที่กำหนด
    public Link2 find(int key) {
        Link2 current = first; // เริ่มที่ 'first'
        while (current != null) {
            if (current.iData == key) {
                return current; // พบลิงค์ที่ตรงกับคีย์
            }
            current = current.next; // ย้ายไปยังลิงค์ถัดไป
        }
        return null; // ไม่พบลิงค์ที่ตรงกับคีย์
    }

    // ลบลิงค์ที่มีคีย์ที่กำหนด
    public Link2 delete(int key) {
        Link2 current = first; // ค้นหาลิงค์
        Link2 previous = first;

        while (current != null) {
            if (current.iData == key) {
                if (current == first) {
                    first = first.next; // เปลี่ยน 'first'
                } else {
                    previous.next = current.next; // ข้ามลิงค์นี้
                }
                return current; // ลิงค์ที่ถูกลบ
            }
            previous = current; // ย้ายไปยังลิงค์ถัดไป
            current = current.next;
        }
        return null; // ไม่พบลิงค์ที่ตรงกับคีย์
    }

    // แสดงรายการลิงค์
    public void displayList() {
        System.out.print("List (first-->last): ");
        Link2 current = first; // เริ่มต้นที่จุดเริ่มต้นของรายการ
        while (current != null) {
            current.displayLink(); // แสดงข้อมูล
            current = current.next; // ย้ายไปยังลิงค์ถัดไป
        }
        System.out.println();
    }
}
