class LinkList2App {
    public static void main(String[] args) {
        LinkList2 theList = new LinkList2(); // สร้างรายการ (ใช้ LinkList2 แทน LinkList)
        theList.insertFirst(22, 2.99); // เพิ่มลิงค์ 4 รายการ
        theList.insertFirst(44, 4.99);
        theList.insertFirst(66, 6.99);
        theList.insertFirst(88, 8.99);
        theList.displayList(); // แสดงรายการ
        Link2 f = theList.find(44); // ค้นหาลิงค์
        if (f != null)
            System.out.println("Found link with key " + f.iData);
        else
            System.out.println("Can't find link");
        Link2 d = theList.delete(66); // ลบลิงค์
        if (d != null)
            System.out.println("Deleted link with key " + d.iData);
        else
            System.out.println("Can't delete link");
        theList.displayList(); // แสดงรายการ
    } // end main()
} // end class LinkList2App

