public class ArrayManipulation {
    public static void main(String[] args) {

        int[] numbers = { 5, 8, 3, 2, 7 };
        String[] names = { "Alice", "Bob", "Charlie", "David" };
        double[] values = new double[4];

        System.out.println("Elements of the numbers array:");
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println("\nElements of the names array:");
        for (String name : names) {
            System.out.println(name);
        }
        values[0] = 1.2;
        values[1] = 2.8;
        values[2] = 3.5;
        values[3] = 4.9;

        int sum = 0;
        for (int number : numbers) {
            sum += number;
        }
        System.out.println("\nSum of elements in the numbers array: " + sum);

        double max = values[0];
        for (int i = 1; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println("Maximum value in the values array: " + max);

        String[] reversedNames = new String[names.length];
        for (int i = 0; i < names.length; i++) {
            reversedNames[i] = names[names.length - 1 - i];
        }

        System.out.println("\nElements of the reversedNames array:");
        for (String reversedName : reversedNames) {
            System.out.println(reversedName);
        }

        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - 1 - i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }

        System.out.println("\nSorted numbers array:");
        for (int number : numbers) {
            System.out.println(number);
        }
    }
}
