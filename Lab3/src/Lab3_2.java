import java.util.Scanner;

public class Lab3_2 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] number = {4, 5, 6, 7, 0, 1, 2};
        System.out.print("target = ");
        int target = kb.nextInt();

        int index = linearSearch(number, target);

        if (index != -1) {
            System.out.println("Output = "+(target+index));
        } else {
            System.out.println("target = "+target+"\nOutput"+"-1");
        }
    }

    public static int linearSearch(int[] number, int target) {
        for (int i = 0; i < number.length; i++) {
            if (number[i] == target) {
                return i;
            }
        }
        return -1;
    }
}