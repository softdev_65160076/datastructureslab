import java.util.Scanner;

public class Lab3_1 {
    private static final String last = null;
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Input = ");
        int x = kb.nextInt();
        mySqrt(x);
        
    }
    public static int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }
        int first = 1, last = x;
        while (first <= last) {
            int mid = first + (last - first) / 2;
            if (mid == x / mid) {
                System.out.println("Output = "+mid);
                return mid;
            } else if (mid > x / mid) {
                last = mid - 1;
            } else {
                first = mid + 1;
            }
        }
        System.out.println("Output = "+last);
        return last;
    }
}